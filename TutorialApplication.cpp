/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
 
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication()
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication()
{
}
//---------------------------------------------------------------------------
void TutorialApplication::createScene()
{
	// Add light which illuminates all objects in the scene regardless of direction
	mSceneMgr->setAmbientLight(Ogre::ColourValue(.25, .25, .25));

	// Create a directional light that gives shadows
	Ogre::Light* pointLight = mSceneMgr->createLight("PointLight");

	// Set the type of light
	pointLight->setType(Ogre::Light::LT_POINT);

	// Set the position of the light
	pointLight->setPosition(250, 150, 250);

	// Color of light that reflects off scene models
	pointLight->setDiffuseColour(Ogre::ColourValue::White);

	// Shininess of light that reflects off scene models
	pointLight->setSpecularColour(Ogre::ColourValue::White);

	// Create an instance of computer graphic model character
	Ogre::Entity* ninjaEntity = mSceneMgr->createEntity("ninja.mesh");

	// Create an object in the scene
	Ogre::SceneNode* ninjaNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("NinjaNode");

	// Add the entity to the object
	ninjaNode->attachObject(ninjaEntity);
}
//---------------------------------------------------------------------------
bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
	// Call the BaseApplication virtual function because we still want its functionality
	bool ret = BaseApplication::frameRenderingQueued(fe);

	// Call the function to process player keyboard and mouse input
	if (!processUnbufferedInput(fe))
		return false;

	return ret;
}
//---------------------------------------------------------------------------
bool TutorialApplication::processUnbufferedInput(const Ogre::FrameEvent& fe)
{
	// Declare static variables that retain value/state between function calls
	// Flag for pressed mouse button
	static bool mouseDownLastFrame = false;

	// Define and initialize the movement timer
	static Ogre::Real toggleTimer = 0.0;

	// Amount to rotate the character body
	static Ogre::Real rotate = 0.13f;

	// Distance to move the character body
	static Ogre::Real move = 250;

	// First method for toggling the scene light
	// Flag for pressed down left mouse button
	bool leftMouseDown = mMouse->getMouseState().buttonDown(OIS::MB_Left);

	// Left mouse button is down and wasn't down last frame
	if (leftMouseDown && !mouseDownLastFrame)
	{
		// Get the directional light
		Ogre::Light* light = mSceneMgr->getLight("PointLight");

		// Set the light visible if not already shining
		light->setVisible(!light->isVisible());
	}

	// Set the last frame flag to current pressed down left mouse button flag
	mouseDownLastFrame = leftMouseDown;

	// Second method for toggling the scene light
	// Subtract the time since the last frame from the movement timer
	toggleTimer -= fe.timeSinceLastFrame;

	// Right mouse button is down and the timer is less than zero
	if ((toggleTimer < 0) && mMouse->getMouseState().buttonDown(OIS::MB_Right))
	{
		// Set the movement timer again
		toggleTimer = .5;

		// Get the directional light
		Ogre::Light* light = mSceneMgr->getLight("PointLight");

		// Set the light visible if not already shining
		light->setVisible(!light->isVisible());
	}

	// Move the character according to player input
	// Define and initialize the character direction 
	Ogre::Vector3 dirVec = Ogre::Vector3::ZERO;

	// Check the I key position
	if (mKeyboard->isKeyDown(OIS::KC_I))

		// Distance to move the character forward
		dirVec.z -= move;

	// Check the K key position
	if (mKeyboard->isKeyDown(OIS::KC_K))

		// Distance to move the character backward
		dirVec.z += move;

	// Check the U key position
	if (mKeyboard->isKeyDown(OIS::KC_U))

		// Distance to move the character up
		dirVec.y += move;

	// Check the O key position
	if (mKeyboard->isKeyDown(OIS::KC_O))

		// Distance to move the character down
		dirVec.y -= move;

	// Check the J key position
	if (mKeyboard->isKeyDown(OIS::KC_J))
	{
		// Check the left shift key position
		if (mKeyboard->isKeyDown(OIS::KC_LSHIFT))

			// Rotate the character left
			mSceneMgr->getSceneNode("NinjaNode")->yaw(Ogre::Degree(5 * rotate));
		else

			// Distance to move the character to the left
			dirVec.x -= move;
	}

	// Check the L key position
	if (mKeyboard->isKeyDown(OIS::KC_L))
	{
		// Check the left shift key position
		if (mKeyboard->isKeyDown(OIS::KC_LSHIFT))

			// Rotate the character right
			mSceneMgr->getSceneNode("NinjaNode")->yaw(Ogre::Degree(-5 * rotate));
		else

			// Distance to move the character to the right
			dirVec.x += move;
	}

	// Move the character
	mSceneMgr->getSceneNode("NinjaNode")->translate(
		dirVec * fe.timeSinceLastFrame,
		Ogre::Node::TS_LOCAL);

	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		} catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
			e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
